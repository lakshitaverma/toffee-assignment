import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter as Router, Route } from "react-router-dom";
import { Provider } from 'react-redux'

import store from './store/index'

import App from './App'
import PersonalDetailsWrapper from './client/components/personalDetails/index'
import ResidentialDetailsWrapper from './client/components/residentialDetails/index'

import registerServiceWorker from './registerServiceWorker'

import '../node_modules/bootstrap/dist/css/bootstrap.min.css'
import '../node_modules/font-awesome/css/font-awesome.min.css'

import  {
    LANDING_PAGE,
    PERSONAL_DETAILS,
    RESIDENTIAL_DETAILS
} from './client/constants/routeConstants'

ReactDOM.render(
    <Provider store={store}>
        <Router>
            <div>
                <Route
                    exact
                    path={LANDING_PAGE}
                    component={App}
                />
                <Route
                    path={PERSONAL_DETAILS}
                    component={PersonalDetailsWrapper}
                />
                <Route
                    path={RESIDENTIAL_DETAILS}
                    component={ResidentialDetailsWrapper}
                />
            </div>
        </Router>
    </Provider>,
    document.getElementById('root')
)

registerServiceWorker()
