import {combineReducers } from 'redux';

import personalDetailsReducer from '../components/personalDetails/personalDetailsReducer'
import residentialDetailsReducer from '../components/residentialDetails/residentialDetailsReducer'

const reducer = combineReducers({
    personalDetailsState: personalDetailsReducer,
    residentialDetailsState: residentialDetailsReducer
})

export default reducer
