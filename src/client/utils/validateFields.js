export function validatePersonalFormFields (values, props) {
    const inputMobileValue = /^[6789]\d{9}$/
    const errors = {};

    if (!values.full_name) {
        errors.full_name = '*Full name is required';
    }

    if (!values.mobile) {
        errors.mobile = '*Mobile is required';
    } else if (values.mobile.length !== 10) {
        errors.mobile = '*Mobile number must be of 10 digits'
    } else if (!inputMobileValue.test(values.mobile)) {
        errors.mobile = '*Mobile number should start with 6, 7, 8 or 9';
    }

    if (!values.email) {
        errors.email = '*Email is required';
    } else if (
        !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i.test(values.email)
    ) {
        errors.email = '*Invalid email address';
    }
    return errors;
}

export function validateResidentialFormFields (values, props) {
    const errors = {};

    if (!values.pincode) {
        errors.pincode = '*Pincode is required';
    } else if (values.pincode.length !== 6) {
        errors.pincode = '*Pincode must be of 6 digits'
    }

    if (!values.city) {
        errors.city = '*City is required';
    }

    if (!values.state) {
        errors.state = '*State is required';
    }
    return errors;
}
