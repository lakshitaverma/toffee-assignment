import React from 'react'

import Hospitalization from './Hospitalization'
import DiagnosticTests from './DiagnosticTests'
import Medicines from './Medicines'
import ExtraItem from './ExtraItem'

/*
    Hospitalization
    DiagnosticTests
    Medicines
    ExtraItem
*/

const CoveredDetails = props => (
    <div>
        <div
            style={{margin: 20}}
            className='text-center'
        >
            <h3>What's covered?</h3>
        </div>
        <div
            style={style.coveredDetailsWrapper}
            className='text-center'
        >
            <Hospitalization />
            <DiagnosticTests />
        </div>
        <div
            style={style.coveredDetailsWrapper}
            className='text-center'
        >
            <Medicines />
            <ExtraItem />
        </div>
    </div>
)

const style = {
    coveredDetailsWrapper: {
        display: 'flex',
        flexDirection: 'row'
    }
}

export default CoveredDetails
