import React from 'react'

const DiagnosticTests = props => (
    <div style={style.diagnosticTestsWrapper}>
        <span style={style.diagnosticTestsText}>
            Diagnostic Tests
        </span><br/>
        <span>covered</span>
    </div>
)

const style = {
    diagnosticTestsWrapper: {
        flex: 6,
        border: '2px solid #eee',
        margin: 10,
        borderRadius: 4,
        padding: 10
    },
    diagnosticTestsText: {
        fontWeight: 'bold',
        fontSize: 18
    }
}

export default DiagnosticTests
