import React from 'react'

const Hospitalization = props => (
    <div style={style.hospitalizationWrapper}>
        <span style={style.hospitalizationText}>
            Hospitalization
        </span><br/>
        <span>Up to Rs. 25000</span>
    </div>
)

const style = {
    hospitalizationWrapper: {
        flex: 6,
        border: '2px solid #eee',
        margin: 10,
        borderRadius: 4,
        padding: 10
    },
    hospitalizationText: {
        fontWeight: 'bold',
        fontSize: 18
    }
}

export default Hospitalization
