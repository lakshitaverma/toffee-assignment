import React from 'react'

const AnnualPremium = props => (
    <div style={style.annualPremiumWrapper}>
        <span style={style.annualPremium}>Rs 425</span><br/>
        <span style={style.annualPremiumText}>
            Annual Premium
        </span>
    </div>
)

const style = {
    annualPremiumWrapper: {
        flex: 6
    },
    annualPremiumText: {
        color: '#eee'
    },
    annualPremium: {
        fontSize: 20,
        fontWeight: 'bold'
    }
}

export default AnnualPremium
