import React from 'react'

const PolicyDuration = props => (
    <div style={{flex: 6}}>
        <span style={{fontSize: 20, fontWeight: 'bold'}}>
            1 year
        </span><br/>
        <span style={{color: '#eee'}}>
            Policy Duration
        </span>
    </div>
)

export default PolicyDuration
