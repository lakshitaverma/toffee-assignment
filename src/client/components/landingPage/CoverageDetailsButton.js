import React from 'react'

import { Link } from "react-router-dom";

import {
    PERSONAL_DETAILS
} from '../../constants/routeConstants'

const CoverageDetailsButton = props => (
    <div
        style={{margin: 20}}
        className='text-center'
    >
        <Link to={PERSONAL_DETAILS}>
            <button
                type="button"
                class="btn btn-danger"
                style={style.button}
            >
                See coverage details
            </button>
        </Link>
    </div>
)

const style = {
    button: {
        padding: '20px 40px'
    }
}

export default CoverageDetailsButton
