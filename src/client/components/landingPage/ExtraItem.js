import React from 'react'

const ExtraItem = props => (
    <div style={style.extraItemWrapper}>
        <span style={style.extraItemText}>
            1 more
        </span>
    </div>
)

const style = {
    extraItemWrapper: {
        flex: 6,
        border: '2px solid #eee',
        margin: 10,
        borderRadius: 4,
        padding: 10,
        paddingTop: 20
    },
    extraItemText: {
        fontWeight: 'bold',
        fontSize: 20
    }
}

export default ExtraItem
