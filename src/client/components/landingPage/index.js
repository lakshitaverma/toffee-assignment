import React from 'react'

import LandingPageHeader from './LandingPageHeader'
import CoveredDetails from './CoveredDetails'
import CoverageDetailsButton from './CoverageDetailsButton'

/*
  LandingPageHeader
  CoveredDetails
  CoverageDetailsButton
*/

const LandingPageWrapper = props => (
  <div>
      <LandingPageHeader />
      <CoveredDetails />
      <CoverageDetailsButton />
  </div>
)

export default LandingPageWrapper
