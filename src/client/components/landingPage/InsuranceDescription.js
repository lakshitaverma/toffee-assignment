import React from 'react'

const InsuranceDescription = props => (
    <span style={style.insuranceDescriptionWrapper}>
        Health insurance to cover all dengue related issues
    </span>
)

const style = {
    insuranceDescriptionWrapper: {
        fontSize: 16,
        color: '#eee'
    }
}

export default InsuranceDescription
