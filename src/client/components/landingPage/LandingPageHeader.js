import React from 'react'

import PolicyDuration from './PolicyDuration'
import AnnualPremium from './AnnualPremium'
import InsuranceDescription from './InsuranceDescription'

/*
  InsuranceDescription
  PolicyDuration
  AnnualPremium
 */

const LandingPageHeader = props => (
  <div
    style={style.landingPageHeaderWrapper}
    className='container-fluid text-center'
  >
    <span style={style.insuranceText}>
      Dengue Insurance
    </span><br/>
    <InsuranceDescription /><br/>
    <div style={style.insurance}>
      <PolicyDuration />
      <AnnualPremium />
    </div>
  </div>
)

const style = {
  landingPageHeaderWrapper: {
    padding: 20,
      backgroundColor: '#e83e8c',
      color: 'white'
  },
  insuranceText: {
    fontSize: 22,
    fontWeight: 'bold'
  },
  insurance: {
    display: 'flex',
    flexDirection: 'row'
  }
}

export default LandingPageHeader
