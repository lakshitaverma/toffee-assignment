import React from 'react'

const Medicines = props => (
    <div style={style.medicinesWrapper}>
        <span style={style.medicinesText}>
            Medicines
        </span><br/>
        <span>covered</span>
    </div>
)

const style = {
    medicinesWrapper: {
        flex: 6,
        border: '2px solid #eee',
        margin: 10,
        borderRadius: 4,
        padding: 10
    },
    medicinesText: {
        fontWeight: 'bold',
        fontSize: 18
    }
}

export default Medicines
