import React from 'react'

const Title = props => (
    <div style={{display: 'flex', margin: 20}}>
        <div
            style={
                props.title === 'Mr'
                ? {...style.selectedTitle, padding: '10px 13px'}
                : {...style.title, padding: '10px 13px'}
            }
            onClick={props.selectedTitle.bind(this, 'Mr')}
        >
            Mr
        </div>
        <div
            style={
                props.title === 'Mrs'
                ? {...style.selectedTitle, padding: '10px 10px', marginLeft: 20}
                : {...style.title, padding: '10px 10px', marginLeft: 20}
            }
            onClick={props.selectedTitle.bind(this, 'Mrs')}
        >
            Mrs
        </div>
        <div
            style={
                props.title === 'Ms'
                ? {...style.selectedTitle, padding: '10px 13px', marginLeft: 20}
                : {...style.title, padding: '10px 13px', marginLeft: 20}
            }
            onClick={props.selectedTitle.bind(this, 'Ms')}
        >
            Ms
        </div>
        <div style={{flex: 6}}></div>
    </div>
)

const style = {
    title: {
        border: '2px solid darkgrey',
        borderRadius: '50%',
        height: 50,
        width: 50
    },
    selectedTitle: {
        border: '2px solid blue',
        borderRadius: '50%',
        height: 50,
        width: 50
    }
}

export default Title
