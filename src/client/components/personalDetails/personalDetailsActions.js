import {
  UPDATE_PERSONAL_DETAILS_FORM_FIELDS,
  UPDATE_TITLE
} from './personalDetailsActionTypes'

export function updatePersonalDetailsFormFields (
  personalDetailsState,
  values
) {
  return {
    type: UPDATE_PERSONAL_DETAILS_FORM_FIELDS,
    personalDetailsState,
    values: values
  }
}

export function updateTitle (personalDetailsState, title) {
  return {
    type: UPDATE_TITLE,
    personalDetailsState,
    title: title
  }
}
