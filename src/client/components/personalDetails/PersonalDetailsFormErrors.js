import React from 'react'

const PersonalDetailsFormErrors = props => (
    <div
        style={{
            margin: 20,
            color: 'red',
            fontSize: 14
        }}
    >
        {
            props.touched.full_name &&
            props.errors.full_name &&
            <div>
                {props.errors.full_name}
            </div>
        }
        {
            props.touched.email &&
            props.errors.email &&
            <div>
                {props.errors.email}
            </div>
        }
        {
            props.touched.mobile &&
            props.errors.mobile &&
            <div>
                {props.errors.mobile}
            </div>
            }
    </div>
)

export default PersonalDetailsFormErrors
