import React from 'react'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import PersonalDetails from './PersonalDetails'

import {
    updatePersonalDetailsFormFields,
    updateTitle
} from './personalDetailsActions'

import {
    RESIDENTIAL_DETAILS
} from '../../constants/routeConstants'

/*
    PersonalDetails
*/


class PersonalDetailsWrapper extends React.Component {
    redirectToResidentialDetailsPage() {
        this.props.history.push(RESIDENTIAL_DETAILS)
    }

    componentDidUpdate(prevProps) {
        if(
            prevProps.personalDetailsState.personalDetailsForm.full_name !==
            this.props.personalDetailsState.personalDetailsForm.full_name
        ) {
            this.redirectToResidentialDetailsPage()
        }
    }

    render () {
        return (
            <PersonalDetails
                personalDetailsState = {this.props.personalDetailsState}
                updateTitle={this.props.actions.updateTitle}
                updatePersonalDetailsFormFields = {this.props.actions.updatePersonalDetailsFormFields}
            />
        )
    }
}

function mapStateToProps (state) {
    return {
        personalDetailsState: state.personalDetailsState
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: bindActionCreators(
            {
                updatePersonalDetailsFormFields,
                updateTitle
            },
            dispatch
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PersonalDetailsWrapper)
