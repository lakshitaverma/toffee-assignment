import {
  UPDATE_PERSONAL_DETAILS_FORM_FIELDS,
  UPDATE_TITLE
} from './personalDetailsActionTypes'

const initialState = {
  personalDetailsForm: {
    title: '',
    full_name: '',
    email: '',
    mobile: ''
  }
}

function personalDetailsReducer (state = initialState, action) {
  switch (action.type) {
    case UPDATE_PERSONAL_DETAILS_FORM_FIELDS:
      return {
        ...state,
        personalDetailsForm: {
          ...state.personalDetailsForm,
          full_name: action.values.full_name,
          email: action.values.email,
          mobile: action.values.mobile
        }
      }

      case UPDATE_TITLE:
      return {
        ...state,
        personalDetailsForm: {
          ...state.personalDetailsForm,
          title: action.title
        }
      }

    default:
      return state
  }
}

export default personalDetailsReducer
