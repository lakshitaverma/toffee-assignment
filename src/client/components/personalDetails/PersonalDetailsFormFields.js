import React from 'react'

import InputField from '../common/InputField'

const PersonalDetailsFormFields = props => (
    <div style={style.personalDetailsFormWrapper}>
        <div style={style.inputFieldWrapper}>
            <InputField
                handleChange={props.handleChange}
                handleBlur={props.handleBlur}
                value={props.values.full_name}
                placeholder='Full Name'
                name="full_name"
            />
        </div>
        <div style={style.inputFieldWrapper}>
            <InputField
                handleChange={props.handleChange}
                handleBlur={props.handleBlur}
                value={props.values.email}
                placeholder='Email'
                name="email"
            />
        </div>
        <div style={{padding: 20, display: 'flex'}}>
            <div style={{flex: 2, marginTop: 1}}>
                <span>+ 91</span>
            </div>
            <div style={{flex: 10}}>
                <InputField
                    handleChange={props.handleChange}
                    handleBlur={props.handleBlur}
                    value={props.values.mobile}
                    placeholder='10-digit Mobile Number'
                    name="mobile"
                    maxLength="10"
                />
            </div>
        </div>
    </div>
)

const style = {
    personalDetailsFormWrapper: {
        margin: 20,
        boxShadow: '0 0 6px 0 rgba(224, 224, 224, 0.72)'
    },
    inputFieldWrapper: {
        padding: 20,
        borderBottom: '2px solid #eee'
    }
}

export default PersonalDetailsFormFields
