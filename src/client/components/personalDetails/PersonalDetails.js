import React from 'react'

import PersonalDetailsForm from './PersonalDetailsForm'

/*
    PersonalDetailsForm
*/

const PersonalDetails = props => (
    <div>
        <div style={{margin: 20}}>
            Now Please enter the name of one of the parents with contact
            details like Email and mobile number
        </div>
        <div style={{margin: 20}}>
            <h3>Father/Mother Details</h3>
        </div>
        <PersonalDetailsForm
            updateTitle={props.updateTitle}
            personalDetailsState = {props.personalDetailsState}
            updatePersonalDetailsFormFields = {props.updatePersonalDetailsFormFields}
        />
        <div style={{margin: 20}}>
            <i
                class="fa fa-question-circle-o"
                aria-hidden="true"
                style={{fontSize: 18}}
            >
            </i>
            <span style={{marginLeft: 10}}>
                Parent's contact details will be used for optional purposes.
            </span>
        </div>
    </div>
)

export default PersonalDetails
