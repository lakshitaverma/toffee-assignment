import React from 'react'

import { withFormik } from 'formik'

import Title from './Title'
import NextButton from './NextButton'
import PersonalDetailsFormErrors from './PersonalDetailsFormErrors'
import PersonalDetailsFormFields from './PersonalDetailsFormFields'

import {
    validatePersonalFormFields
} from '../../utils/validateFields'

/*
    PersonalDetailsFormErrors
    Title
    PersonalDetailsFormFields
    NextButton
*/

class PersonalDetailsForm extends React.Component {
    selectedTitle (title) {
        this.props.updateTitle(this.props.personalDetailsState, title)
    }

    render() {
        return (
            <form onSubmit={this.props.handleSubmit}>
                <PersonalDetailsFormErrors
                    touched = {this.props.touched}
                    errors = {this.props.errors}
                />
                <Title
                    title={this.props.personalDetailsState.personalDetailsForm.title}
                    selectedTitle={this.selectedTitle.bind(this)}
                />
                <PersonalDetailsFormFields
                    handleChange={this.props.handleChange}
                    handleBlur={this.props.handleBlur}
                    values={this.props.values}
                />
                <NextButton
                    isSubmitting={this.props.isSubmitting}
                />
            </form>
        )
    }
}


export default withFormik({
    mapPropsToValues: props => {
        return {
            full_name: '',
            email: '',
            mobile: ''
        }
    },
    validate: (values, props) => {
        const errors = validatePersonalFormFields(values, props)
        return errors
    },
    handleSubmit: (values, { props, setSubmitting }) => {
        props.updatePersonalDetailsFormFields (
            props.personalDetailsState,
            values
        )
    },
    displayName: 'PersonalDetailsForm' // helps with React DevTools
    })(PersonalDetailsForm)
