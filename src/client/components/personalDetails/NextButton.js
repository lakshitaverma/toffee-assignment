import React from 'react'

const NextButton = props => (
    <div style={{margin: 20}}>
        <button
            type="submit"
            disabled={props.isSubmitting}
            style={style.button}
        >
            Next
            <i
                className="fa fa-long-arrow-right"
                aria-hidden="true"
                style={style.icon}
            >
            </i>
        </button>
    </div>
)

const style = {
    button: {
        color: '#fff',
        backgroundColor: '#dc3545',
        borderColor: '#dc3545',
        width: '100%',
        padding: 20,
        fontSize: 20
    },
    icon: {
        marginLeft: 20,
        fontSize: 22
    }
}

export default NextButton
