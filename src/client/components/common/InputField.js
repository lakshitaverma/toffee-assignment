import React from 'react'

const InputField = props => (
    <input
        type="text"
        name={props.name}
        onChange={props.handleChange}
        onBlur={props.handleBlur}
        value={props.value}
        placeholder={props.placeholder}
        maxLength={props.maxLength}
        style={style.inputField}
    />
)

const style = {
    inputField: {
        outline: 0,
        border: 0,
        width: '100%'
    }
}

export default InputField
