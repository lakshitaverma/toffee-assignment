import React from 'react'

import ResidentialDetailsForm from './ResidentialDetailsForm'
import ResidentialDetailsCard from './ResidentialDetailsCard'

/*
    ResidentialDetailsCard
    ResidentialDetailsForm
*/

const ResidentialDetails = props => (
    <div>
        <ResidentialDetailsCard />
        <div style={{margin: 20}}>
            <span>
                Mythri, please enter your residential location.
            </span>
        </div>
        <div style={{margin: 20}}>
            <h3>Residential Location</h3>
        </div>
        <ResidentialDetailsForm
            residentialDetailsState = {props.residentialDetailsState}
            updateResidentialDetailsFormFields = {props.updateResidentialDetailsFormFields}
        />
    </div>
)

export default ResidentialDetails
