import React from 'react'

const PolicyDetails = props => (
    <div
        style={{
            padding: '5px 20px',
            backgroundColor: 'pink'
        }}
    >
        <span style={style.policy}>
            POLICY NUMBER
        </span><br/>
        <span style={style.policyNumber}>
            0101 98138 73890
        </span>
    </div>
)

const style = {
    policy: {
        fontSize: 14,
        color: '#fff'
    },
    policyNumber: {
        fontSize: 20,
        color: '#eee'
    }
}

export default PolicyDetails
