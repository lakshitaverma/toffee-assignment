import React from 'react'

import InsuranceDetails from './InsuranceDetails'
import PolicyDetails from './PolicyDetails'
import UserDetails from './UserDetails'

/*
    InsuranceDetails
    PolicyDetails
    UserDetails
*/

const ResidentialDetailsCard = props => (
    <div style={style.residentialDetailsCardWrapper}>
        <InsuranceDetails />
        <PolicyDetails />
        <UserDetails />
    </div>
)

const style = {
    residentialDetailsCardWrapper: {
        backgroundColor: '#f64a8a',
        margin: 20,
        borderRadius: 10
    }
}

export default ResidentialDetailsCard
