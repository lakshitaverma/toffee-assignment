import React from 'react'

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import ResidentialDetails from './ResidentialDetails'

import  {
    updateResidentialDetailsFormFields
} from  './residentialDetailsActions'

import {
    LANDING_PAGE
} from '../../constants/routeConstants'

/*
    ResidentialDetails
*/

class ResidentialDetailsWrapper extends React.Component {
    redirectToLandingPage() {
        this.props.history.push(LANDING_PAGE)
    }

    componentDidUpdate(prevProps) {
        if(
            prevProps.residentialDetailsState.residentialDetailsForm.pincode !==
            this.props.residentialDetailsState.residentialDetailsForm.pincode
        ) {
            this.redirectToLandingPage()
        }
    }

    render() {
        return (
            <ResidentialDetails
                residentialDetailsState = {this.props.residentialDetailsState}
                updateResidentialDetailsFormFields = {this.props.actions.updateResidentialDetailsFormFields}
            />
        )
    }
}

function mapStateToProps (state) {
    return {
        residentialDetailsState: state.residentialDetailsState
    }
}

function mapDispatchToProps (dispatch) {
    return {
        actions: bindActionCreators(
            {
                updateResidentialDetailsFormFields
            },
            dispatch
        )
    }
}

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(ResidentialDetailsWrapper)
