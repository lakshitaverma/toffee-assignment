import React from 'react'

const InsuranceDetails = props => (
    <div style={{padding: 20}}>
        <span style={style.white}>
            Dengue Insurance
        </span><br/>
        <span style={style.insuranceDate}>
            June 2017 - June 2018
        </span>
    </div>
)

const style = {
    white: {
        color: '#fff'
    },
    insuranceDate: {
        color: '#eee',
        fontSize: 14
    }
}

export default InsuranceDetails
