import React from 'react'

const ResidentialDetailsFormErrors = props => (
    <div
        style={{
            margin: 20,
            color: 'red',
            fontSize: 14
        }}
    >
        {
            props.touched.pincode &&
            props.errors.pincode &&
            <div>
                {props.errors.pincode}
            </div>
        }
        {
            props.touched.state &&
            props.errors.state &&
            <div>
                {props.errors.state}
            </div>
        }
        {
            props.touched.city &&
            props.errors.city &&
            <div>
                {props.errors.city}
            </div>
            }
    </div>
)

export default ResidentialDetailsFormErrors
