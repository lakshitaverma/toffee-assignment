import {
    UPDATE_RESIDENTIAL_DETAILS_FORM_FIELDS
} from './residentialDetailsActionTypes'

const initialState = {
    residentialDetailsForm: {
        pincode: '',
        city: '',
        state: ''
    }
}

function residentialDetailsReducer (state = initialState, action) {
    switch (action.type) {
        case UPDATE_RESIDENTIAL_DETAILS_FORM_FIELDS:
            return {
                ...state,
                residentialDetailsForm: {
                    ...state.residentialDetailsForm,
                    pincode: action.values.pincode,
                    city: action.values.city,
                    state: action.values.state
                }
            }

        default:
            return state
    }
}

export default residentialDetailsReducer
