import React from 'react'

import InputField from '../common/InputField'

const ResidentialDetailsFormFields = props => (
    <div style={style.residentialDetailsFormWrapper}>
        <div style={style.inputFieldWrapper}>
            <InputField
                handleChange={props.handleChange}
                handleBlur={props.handleBlur}
                value={props.values.pincode}
                placeholder='Pin code'
                name="pincode"
                maxLength="6"
            />
        </div>
        <div style={style.inputFieldWrapper}>
            <InputField
                handleChange={props.handleChange}
                handleBlur={props.handleBlur}
                value={props.values.state}
                placeholder='State'
                name="state"
            />
        </div>
        <div style={{padding: 20, display: 'flex'}}>
            <div style={{flex: 10}}>
                <InputField
                    handleChange={props.handleChange}
                    handleBlur={props.handleBlur}
                    value={props.values.city}
                    placeholder='city'
                    name="city"
                />
            </div>
            <div style={{flex: 2}}>
                <button type="submit" disabled={props.isSubmitting}>
                    <i
                        className="fa fa-long-arrow-right"
                        aria-hidden="true"
                        style={style.icon}
                    >
                    </i>
                </button>
            </div>
        </div>
    </div>
)

const style = {
    residentialDetailsFormWrapper: {
        margin: 20,
        boxShadow: '0 0 6px 0 rgba(224, 224, 224, 0.72)'
    },
    inputFieldWrapper: {
        padding: 20,
        borderBottom: '2px solid #eee'
    },
    icon: {
        fontSize: 26,
        color: 'grey'
    }
}

export default ResidentialDetailsFormFields
