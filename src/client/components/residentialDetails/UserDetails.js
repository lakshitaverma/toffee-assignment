import React from 'react'

const UserDetails = props => (
    <div style={{padding: 20}}>
        <span style={style.white}>
            Sonia Babu
        </span><br/>
        <span style={style.email}>
            mythrineedinsurance@gmail.com
        </span>
    </div>
)

const style = {
    white: {
        color: '#fff'
    },
    email: {
        fontSize: 14,
        color: '#eee'
    }
}

export default UserDetails
