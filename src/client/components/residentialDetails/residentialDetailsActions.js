import {
    UPDATE_RESIDENTIAL_DETAILS_FORM_FIELDS
} from './residentialDetailsActionTypes'

export function updateResidentialDetailsFormFields (
    residentialDetailsState,
    values
) {
    return {
        type: UPDATE_RESIDENTIAL_DETAILS_FORM_FIELDS,
        residentialDetailsState,
        values: values
    }
}
