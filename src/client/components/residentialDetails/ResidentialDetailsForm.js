import React from 'react'

import { withFormik } from 'formik'

import ResidentialDetailsFormErrors from './ResidentialDetailsFormErrors'
import ResidentialDetailsFormFields from './ResidentialDetailsFormFields'

import {
    validateResidentialFormFields
} from '../../utils/validateFields'

/*
    ResidentialDetailsFormErrors
    ResidentialDetailsFormFields
*/

const ResidentialDetailsForm = props => {
    return (
        <form onSubmit={props.handleSubmit}>
            <ResidentialDetailsFormErrors
                touched = {props.touched}
                errors = {props.errors}
            />
            <ResidentialDetailsFormFields
                handleChange={props.handleChange}
                handleBlur={props.handleBlur}
                values={props.values}
                isSubmitting={props.isSubmitting}
            />
        </form>
    )
}

export default withFormik({
    mapPropsToValues: props => {
        return {
            pincode: '',
            city: '',
            state: ''
        }
    },
    validate: (values, props) => {
        const errors = validateResidentialFormFields(values, props)
        return errors
    },
    handleSubmit: (values, { props, setSubmitting }) => {
        props.updateResidentialDetailsFormFields (
            props.residentialDetailsState,
            values
        )
    },
    displayName: 'ResidentialDetailsForm' // helps with React DevTools
    })(ResidentialDetailsForm)
